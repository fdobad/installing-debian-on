_installing on mac book pro 5,1_

# Steps
0. Power on holding the 'alt key' while booting, select usb from the menu to boot from the latest debian (gnome live non-free) usb

1. On the GRUB menu select the first option: Start live Kernel 

2. Seeing an empty background means the monitor is displaying the 2nd screen; press 2dary click to enter 'Display Settings'; Configure to use just the 2nd one (although it's easier to just mirror). This is caused by the laptop having 2 gpu's

3. Plug to the internet before running the installer; use a network cable (RJ45) or usb tethering

4. Launch the installer located as the first app (top), on the (left) launcher

5. In the partitions steps, make sure to set at least the same amount of swap than RAM to hibernate correctly (probably 8Gb, check with `free` on a terminal)

6. On first boot: Edit apt sources (`$ sudo -e /etc/apt/sources.list`) to include contrib and non-free sources; also buster backports (example for bullseye):
```
    # See https://wiki.debian.org/SourcesList for more information.
    deb http://deb.debian.org/debian bullseye main contrib non-free
    deb-src http://deb.debian.org/debian bullseye main contrib non-free

    deb http://deb.debian.org/debian bullseye-updates main contrib non-free
    deb-src http://deb.debian.org/debian bullseye-updates main contrib non-free

    deb http://security.debian.org/debian-security/ bullseye-security main contrib non-free
    deb-src http://security.debian.org/debian-security/ bullseye-security main contrib non-free

    deb http://deb.debian.org/debian buster-backports main contrib non-free
    deb-src http://deb.debian.org/debian buster-backports main contrib non-free
```
7. Update, install non-free wifi and backported nvidia drivers:
```
	$ sudo apt update
	$ sudo apt install firmware-b43-installer
	$ sudo apt install -t buster-backports nvidia-legacy-340xx-driver
```
8. Mark nvidia packages to not get updated
```
    $ sudo apt-mark hold *nvidia*
```
9. Beware that upgrading kernels could break the system, make sure to install before any kernel upgrade:
9.1.
```
    $ sudo apt install linux-headers-amd64
```
9.2.
If you get the crash anyway: 
```
systemd-modules-load: Error running install command 'modprobe -i nvidia-legacy-340xx ' for module nvidia: retcode 1
systemd-modules-load: Failed to insert module 'nvidia': Invalid argument
systemd-modules-load: modprobe: FATAL: Module nvidia-legacy-340xx not found in directory /lib/modules/5.10.0-21-amd64
```
At least using gnome, you can get a tty by pressing `Ctrl+Alt+F1` (or F2, F3,... until you get a cmd login)(probably you need to press the `fn` key, so: `Ctrl+Alt+fn+F1`). Else reboot and get into recovery mode.  
Then  
```
    # get a wifi conection
    $ nmtui
    # reinstall
    $ sudo apt reinstall linux-headers-amd64 nvidia-legacy-340xx-driver nvidia-legacy-340xx-kernel*
```
9.3. Make sure root has a password to get into recovery mode
```
    $ sudo passwd root
```
10. Get 4 fingers touchpad support
```
    # download the latest release from: https://github.com/JoseExposito/touchegg/releases
    $ sudo apt install  ./Downloads/touchegg_2.0.15_amd64.deb
```
11. Must have extensions
- [Hibernate button](https://extensions.gnome.org/extension/755/hibernate-status-button/)
- [Day/night theme-switcher](https://extensions.gnome.org/extension/3936/adwaita-theme-switcher/)

Reboot and enjoy!
