# Usb stick preparation
Choose the right .ISO version for you, usually a non-free version including a window manager is the easiest way to go, choose from:
```
https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current-live/amd64/bt-hybrid/
```
If you don't know which to pick go for GNOME on a newer laptop and XFCE for older. A 8Gb stick should more than enough.

The non-free version includes firmware that should enable wifi during the installer, if not usb tethering with Android (<10?) or a usb to RJ45 adapter should work. Having an internet connection during install saves you a lot of hassle.

If you wish a headless install go for the standard version; or for afterwards installing a desktop manager and a window manager, for example, lightdm & i3.


# From debian
```
# Replace sdb with the correct device
sudo umount /dev/sdb1
sudo cp debian-live-11.3.0-amd64-standard+nonfree.iso /dev/sdb
sudo sync
```

# From macOS
```
# convert ISO to IMG:  
$ hdiutil convert debian-live-11.3.0-amd64-standard+nonfree.iso -format UDRW -o debian-live-11.3.0-amd64-standard+nonfree.img

# hdiutil will attach .dmg to filename, so rename:
$ mv debian-live-11.3.0-amd64-standard+nonfree.img.dmg debian-live-11.3.0-amd64-standard+nonfree.img

# attach USB stick & check which /dev/diskX it is assigned with:  
$ diskutil list

# unmount USB stick  
$ diskutil unmountDisk /dev/diskX

# copy image to USB stick  
$ sudo dd if=debian-live-11.3.0-amd64-standard+nonfree.img of=/dev/diskX bs=1m
```
 
 # From windows
 - Create bootable USB drives the easy way using rufus: https://rufus.ie/  

 
