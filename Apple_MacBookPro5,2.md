# InstallingDebianOn Apple MacBookPro 5,2 17-inch

The objective is a Debian Bookworm with GNOME WM, nvidia drivers working and 4 finger touchpad enabled.

# Hardware
Buy any SSD and replace the old hard drive before beginning, having 8Gb RAM is recommended

# Usb stick preparation
Easiest way for the source is the live .ISO version. Get it from:
```
https://cdimage.debian.org/debian-cd/current-live/amd64/bt-hybrid/
```
Choose the gnome version!  
Netinstall was also sucessfully ran  

# Install
Boot the laptop with the usbdrive plugged in, hold the 'alt' key, until the select boot medium, select the 'EFI boot' usb.

Select the live system or the graphical installer, but beware that because the double video card, sometimes it get messed which display to put the installer window, so you'll get stuck on a mouse only blank desktop (that has a hidden space to the left). Be ready to (have a mouse?) right click, enter display settings, swap screen primary display, then you can turn the other display off. Else blindly try to drag the live installer menu: move to the center of the invisible left display, hold alt+primary click, then go back right if you did catch it.

If this happens on first reboot after the installer, just enter your username and password blindly, inside the session you can restore it!

Alternatives:
- Boot into rescue mode and edit `/etc/gdm3/daemon.conf` uncommenting the 'Enabling automatic login' section
```
AutomaticLoginEnable = true
AutomaticLogin = <your_user>
```

- TODO: Rescue mode, fix it with xrandr

Make sure to leave double your ram as swap space, and to remember your root and user credentials

# After install steps

## WiFi driver

1. Identify hardware, further [read](https://wiki.debian.org/HowToIdentifyADevice/PCI#pci-id)  
```
$ lspci -nn | grep Network
04:00.0 Network controller [0280]: Broadcom Inc. and subsidiaries BCM4322 802.11a/b/g/n Wireless LAN Controller [14e4:432b] (rev 01)
```

Search for 'debian BCM4322 432b' got me [here](https://wiki.debian.org/bcm43xx), where they recommended [b43](https://wiki.debian.org/bcm43xx#b43_and_b43legacy) but with partial support

2. Install the non-free software
If fails, make sure contrib & non-free are enabled on your repos
	
```
$ sudo apt install firmware-b43-installer
```

3. Optional, add some kernel params to b43 driver to enhance support

Still had some troubles, slow connectivity, a few systems hangups, `sudo dmesg` shows:
```
[ 17.321407] b43-phy0 ERROR: Fatal DMA error: 0x00000400, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000
[ 17.321625] b43-phy0 ERROR: This device does not support DMA on your system. It will now be switched to PIO.
[ 17.321832] b43-phy0: Controller RESET (DMA error) ...
[ 17.612527] b43-phy0: Loading firmware version 784.2 (2012-08-15 21:35:19)
[ 17.680738] b43-phy0 warning: Forced PIO by use_pio module parameter. This should not be needed and will result in lower performance.
```

Solve it adding this Kernel param

```
$ sudo -e /etc/default/grub

# append the following
GRUB_CMDLINE_LINUX_DEFAULT="text b43.pio=1 b43.qos=0"

$ sudo update-grub
```

## NVIDIA drivers
### Prepare sources
```
# Append sid source to the end of the sources file
# optional enable contrib to the other sources!
$ sudo -e /etc/apt/sources.list

deb http://deb.debian.org/debian/ sid main contrib non-free
deb-src http://deb.debian.org/debian/ sid main contrib non-free

# Create a preferences file to only install nvidia-legacy drivers and keep your distribution stable
$ sudo -e /etc/apt/preferences

Package: *
Pin: release a=stable
Pin-Priority: 700

Package: *
Pin: release a=testing
Pin-Priority: 650

Package: *
Pin: release a=unstable,sid
Pin-Priority: 600
```

### Install
```
$ sudo apt update
$ sudo apt install linux-headers-amd64
$ sudo apt install -t sid nvidia-legacy-340xx-driver
```
### Precaution
```
$ sudo apt-mark hold *nvidia*
```
### Optional: Disable annoying splash logo
```
$ sudo -e /etc/X11/xorg.conf.d/10-nvidia.conf

Section "Device"
    Identifier	"Device0"
    Driver	"nvidia"
    VendorName	"NVIDIA Corporation"
    Option	"NoLogo" "True"
EndSection
```

## Multitouch support for GNOME
### Install from .deb file
Download the latest (release)[https://github.com/JoseExposito/touchegg/releases] 
```
$ sudo apt install ./touchegg_2.0.14.deb
```
### Enable the gnome extension
 1. Install the gnome browser extension on your favorite most reliable browser (that is firefox)
 2. Go to https://extensions.gnome.org/extension/4033/x11-gestures/ (or search for touchegg)
 3. Install it by moving the switch to ON
 4. Restart X... just reboot

### references
- (repo)[https://github.com/JoseExposito/touchegg#ubuntu-debian-and-derivatives]
- (gnome extension)[https://extensions.gnome.org/extension/4033/x11-gestures/]

## Swap
It's very important for the system to sleep then hibernate properly, else you'll get frustrated out a system that only suspends for a few hours.

Check if it's permanently enabled:
```
$ sudo swapon --show

# returns like this
NAME      TYPE      SIZE USED PRIO
/dev/sda2 partition 8.8G 796K   -2
```
And permanently mounted with
```
$ cat /etc/fstab

# there should be a line like this:
UUID="cda73...etc...d4ff36"     none    swap    sw      0       0
```

If failed do this: https://wiki.debian.org/Swap 
Althought I think it's better and easier to make a permanent partition with gparted

## If you get this error on `journalctl`
```
kernel: clocksource: timekeeping watchdog on CPU1: Marking clocksource 'tsc' as unstable because the skew is too large:
TSC found unstable after boot, most likely due to broken BIOS. Use 'tsc=unstable'.

$ sudo -e /etc/default/grub

# append the following
GRUB_CMDLINE_LINUX_DEFAULT="text b43.pio=1 b43.qos=0 tsc=unstable"

$ sudo update-grub
```


##### (optional) Steam games requires i386
```
$ sudo dpkg --add-architecture i386
$ sudo apt install -t buster-backports nvidia-legacy-340xx-driver-libs-i386
$ sudo apt install steam
```
