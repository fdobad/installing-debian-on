# Debian Buster on a Dell XPS 13 9350 laptop

1. Update your EFI/BIOS

2. Use this EFI/BIOS options:
 - Change "RAID On" to "AHCI"
 - To access bios:
    - Press \<F12> when booting  
    - Or in windows hold shift when clicking the restart button, then ~~>Advanced>EFI settings~~  


3. Make a usb installer, https://cdimage.debian.org/debian-cd/current-live/amd64/bt-hybrid/  
 - Using the latest live amd64 distribution with GNOME, check your architecture!

 - For burning in windows 10, used rufus https://rufus.ie/  
 Tested: burning in dd mode in a 4GB usb stick
 
 
4. Restart and boot from the usb, connect to internet, start the installer  
 Tested "install along side windows10", downsized the windows partition (the biggest one), updated efi/grub correctly

# Graphic driver lags
Add this kernel param `i915.enable_dc=0`, disables power management
```bash
sudo -e /etc/default/grub

GRUB_CMDLINE_LINUX_DEFAULT="quiet splash i915.enable_dc=0"

sudo update-grub
```

# Wifi needs non-free repos
You can **share a internet conection using Android USB tethering to avoid the non-free repos during install**.

To install the propietary driver:  
```bash
sudo apt-add-repository non-free
sudo apt update
sudo apt install firmware-brcm80211 firmware-misc-nonfree
```
Explanation
```bash
$ lspci | grep Wireless
3a:00.0 Network controller: Broadcom Limited BCM4350 802.11ac Wireless Network Adapter (rev 08)
```
Internet search for "debian buster BCM4350" will point you to https://packages.debian.org/buster/firmware-brcm80211 having the proper driver "Broadcom BCM4350 rev 0-4 firmware (brcm/brcmfmac4350-pcie.bin)"

You could revert the non-free repos
 - Editing /etc/apt/sources.list removing all 'non-free'
 - Or from "Software" application: Menu > Software repositories > Non-DFSG-compatible Software (non-free) disabling its checkbox

# Beware of windows when dualbooting
If you dualboot along Windows 10, windows ~every~ many times a major update will disconnect your efi-grub manager, fix it from windows 10, executing this command in **cmd.exe as administrator**:  
```
bcdedit /set {bootmgr} path \EFI\debian\grubx64.efi
```
Notice the amd64 architecture (x64).

# OPTIONAL section: Making things more comfy

## Making grub letters bigger
to do

## On System Settings:
 - Devices > Mouse & Touchpad:
   - Enable the touchpad
   - Tap to click
 - For DragonFly USB DAC: Fn+Keys work, the DAC changes playback quality according to the file played.
   - Sounds: Choose DragonFly Red Analog Output on Sounds  
   - VLC: Audio > Audio Device > "AudioQuest Dragon...USB Audio Hardware device with all software conversion"  
   
## Tweaks App
For GNOME,  
 - Increase the font size on >Fonts : all 4 fonts to 12  
 - Enable minimize on >Window Titlebars : Minimize switch on

## Day/Night mode switcher
For GNOME, make a day/night theme changing keyboard shortcuts with these two files:
- Shortcut:`Super+dia`, file:`.local/share/applications/dia.desktop`:  

.jupyter/lab/user-settings/@jupyterlab/apputils-extension/themes.jupyterlab-settings
    "theme": "JupyterLab Dark",
    "theme": "JupyterLab Light",
 
```
[Desktop Entry]
Version=1.0
Name=dia
Comment=Change gnome theme to Adwaita
Exec=gsettings set org.gnome.desktop.interface gtk-theme "Adwaita"
StartupNotify=true
Terminal=true
Type=Application
Icon=/usr/share/icons/Adwaita/scalable/status/display-brightness-symbolic.svg
Categories=Application;
```  
 
- Shortcut:`Super+noche`, file:`.local/share/applications/noche.desktop`:

```
[Desktop Entry]
Version=1.0
Name=noche
Comment=Change gnome theme to Adwaita-dark
Exec=gsettings set org.gnome.desktop.interface gtk-theme "Adwaita-dark"
StartupNotify=true
Terminal=true
Type=Application
Icon=/usr/share/icons/Adwaita/scalable/status/night-light-symbolic.svg
Categories=Application;
```
