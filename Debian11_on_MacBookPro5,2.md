# WIFI
## WiFi driver
1. Identify hardware, further [read](https://wiki.debian.org/HowToIdentifyADevice/PCI#pci-id)  
	```
	$ lspci -nn | grep Network
	04:00.0 Network controller [0280]: Broadcom Inc. and subsidiaries BCM4322 802.11a/b/g/n Wireless LAN Controller [14e4:432b] (rev 01)
	```
Search for 'debian BCM4322 432b' got me [here](https://wiki.debian.org/bcm43xx), where they recommended [b43](https://wiki.debian.org/bcm43xx#b43_and_b43legacy) but with partial support
2. Install the non-free software
	
	```
	$ sudo apt install firmware-b43-installer
	```
Still had some troubles, slow connectivity, some systems freezes, on `dmesg`:
> [   17.321407] b43-phy0 ERROR: Fatal DMA error: 0x00000400, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000
> [   17.321625] b43-phy0 ERROR: This device does not support DMA on your system. It will now be switched to PIO.
> [   17.321832] b43-phy0: Controller RESET (DMA error) ...
> [   17.612527] b43-phy0: Loading firmware version 784.2 (2012-08-15 21:35:19)
> [   17.680738] b43-phy0 warning: Forced PIO by use_pio module parameter. This should not be needed and will result in lower performance.

3. Add some kernel params to b43 driver to enhance support
	```
	# vim /etc/default/grub
	GRUB_CMDLINE_LINUX_DEFAULT="text b43.pio=1 b43.qos=0"

# NVIDIA
## edit apt sources
Add non-free contrib 
Add buster back ports
sudo apt install -t buster-backports nvidia-legacy-340xx-driver
apt-mark hold *nvidia*



# Multitouch support for GNOME
## Install from repo FAILED 
	```
	$ sudo add-apt-repository ppa:touchegg/stable
	$ sudo apt update
	$ sudo apt install touchegg
	```
## Alternatively Install from .deb file
 - Download the latest (release)[https://github.com/JoseExposito/touchegg/releases]
	```
	$ cd ~/Downloads # Or to the path where the deb package is placed at
	$ sudo apt install ./touchegg_*.deb # Install the package
	```
## Enable the gnome extension
 - Enable the browser extension
 - Check your shell version
	- for v3 check /usr/share/gnome/gnome-version.xml
	- download the correct extension
	- check the uuid value on the metadata.json file
	- extract to ~/.local/share/gnome-shell/extensions/x11gestures@joseexposito.github.io
	
	
## references
- (repo)[https://github.com/JoseExposito/touchegg#ubuntu-debian-and-derivatives]
- (gnome extension)[https://extensions.gnome.org/extension/4033/x11-gestures/]

# Steam
sudo dpkg --add-architecture i386
sudo apt install -t buster-backports nvidia-legacy-340xx-driver-libs-i386
sudo apt install steam

