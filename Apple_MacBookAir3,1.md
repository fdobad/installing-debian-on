# InstallingDebianOn Apple MacBookAir 3,1

The objective is a minimal memory intensive system with i3 window manager (also using lightDM). A 200 to 300MByte in ram at idle is achieved.

# HW Specs
MacBook Air 11.6" 1.4GHz Core 2 Duo (A1370, MC505LL/A, MacBookAir3,1) - Late 2010

MacBookAir3,1 / Mac-942452F5819B1C1B

2 GB of 1066 MHz DDR3 SDRAM

# Get the ISO
The choosen .ISO version was the standard (no gui) non-free version. Get it from:
```
https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current-live/amd64/bt-hybrid/
```
The non-free version should enable wifi during the installer, if not usb tethering with Android (<10?) or a usb to RJ45 adapter should work. Being connected to the internet during install saves you a lot of trouble.

# Install
Boot the laptop with the usbdrive plugged in, hold the 'alt' key, until the select boot medium, select the 'EFI boot' usb.

On the grub menu, choose Graphical Installer, make sure you enable an internet connection because it automates many things.

Follow the steps, make sure to apart some 4gb of swap space to hibernate securely, and to remember the root password.

# After installing steps  
## Give your user privileges
```
sudo usermod -aG sudo fdo
```
## Networking
### With out fancy network-manager
1. Enable dhcp by making this file 
```
sudo -e /etc/systemd/network/dhcp.network 

[Match]
Name=*
	
[Network]
DHCP=yes
```

2. Make sure the proper interfaces are set: Get them with `ip a`, erase the default `eth0` or similar:
```
sudo -e /etc/network/interfaces.d/setup
sudo -e /etc/network/interfaces

auto lo
iface lo inet loopback

allow-hotplug wlp1s0b1
iface wlp1s0b1 inet dhcp
```

3. Restart networking services
```
sudo systemctl restart networking
sudo systemctl restart systemd-networking
```
Wifi should be enable by now, if not:
```
~sudo apt install brcmsmac~
sudo apt install firmware-brcm80211

# disable the b43 driver
echo "blacklist b43" | sudo tee /etc/modprobe.d/b43.conf
sudo depmod -ae
sudo update-initramfs -u
```
4. Install a Network Manager:

Ligther but ugly cli gui `nmtui`:
```
sudo apt install network-manager
```
5. Usage
```
systemctl restart networking NetworkManager systemd-networkd
nmtui
```
### With fancy network-manager
With nice GUI `nm-applet` (and bluetooth):
```
sudo apt install network-manager-gnome blueman
```

## Nvidia drivers
1. Kernel corrections

1.1 Make a new file
```
sudo -e /etc/grub.d/01_enable_vga.conf

cat << EOF
setpci -s "00:17.0" 3e.b=8
setpci -s "02:00.0" 04.b=7
EOF

sudo chmod 755 /etc/grub/01_enable_vga.conf 
```

1.2 Insert kernel params
```
sudo -e /etc/default/grub

# append to this line:
GRUB_CMDLINE_LINUX_DEFAULT="quiet splash intremap=off acpi_osi=Darwin"
```

1.3 Update
```
sudo update-grub
```

2. Apt install

2.1 Enable buster backports
```
sudo -e /etc/apt/sources.list

# Append this at the end.
deb http://deb.debian.org/debian buster-backports main contrib non-free
deb-src http://deb.debian.org/debian buster-backports main contrib non-free
```
(Now is a good time to check if contrib & non-free are enabled for the rest of the repos)

2.2 Install
```
sudo apt update
sudo apt install linux-headers-amd64
sudo apt install -t buster-backports nvidia-legacy-340xx-driver
```
2.3 Precaution
```
sudo apt-mark hold *nvidia*
```
3. Enable brightness control, disable splash
```
sudo -e /etc/X11/xorg.conf.d/10-nvidia-brightness.conf

Section "Device"
    Identifier	"Device0"
    Driver	"nvidia"
    VendorName	"NVIDIA Corporation"
    Option	"RegistryDwords" "EnableBrightnessControl=1"
    Option	"NoLogo" "True"
    BusID	"PCI:02:0:0"
EndSection
```

## DesktopManager and WindowManager
1. Install
```
sudo apt install xserver-xorg lightdm i3wm i3status xbacklight 
```
2. Do first login and follow the tutorial

3. Some more config

3.1 Enable brightness buttons
```
vim .config/i3/config

# screen brightness
bindcode 232 exec --no-startup-id xbacklight -5%
bindcode 233 exec --no-startup-id xbacklight +5%
```
3.2 Make sure to disable the i3lock that wasn't installed
```
# exec --no-startup-id xss-lock --transfer-sleep-lock -- i3lock --nofork
```
Control the screensaver with `xset`, for example:  `xset -q`, `xset s 600 600`

3.3 If you installed the gnome network manager don't disable this
```
# exec --no-startup-id nm-applet
```
3.4 If you installed the bluetooth gtk manager
```
exec --no-startup-id blueman
```
## Touchpad
Enable tap to click, 1,2 & 3 buttons emulation
```
sudo -e /etc/X11/xorg.conf.d/30-touchpad.conf

Section "InputClass"
	Identifier "touchpad"
	Driver "libinput"
	MatchProduct "bcm5974"
	MatchIsTouchpad "true"
	Option "NaturalScrolling" "true"
	Option "Tapping" "on"
EndSection
```
Altought `touchegg` seems a nicer alternative for 4 finger events.

## Nicer Font
```
sudo apt install fonts-terminus

# set it in .config/i3/config
vim .config/i3/config

# and as default terminal font
vim .Xresources
xterm font terminus 12
```

## Swap
It's very important for the system to sleep then hibernate properly, else you'll get frustrated out a system that only suspends for a few hours.

Check if it's permanently enable:
```
sudo swapon --show

# returns like this
NAME      TYPE      SIZE USED PRIO
/dev/sda2 partition 8.8G 796K   -2
```
And permanently mounted with
```
cat /etc/fstab

# there should be a line like this:
UUID="cda73...etc...d4ff36"     none    swap    sw      0       0
```

If failed do this: https://wiki.debian.org/Swap 
Althought I think it's better to make a permanent partition with gparted
